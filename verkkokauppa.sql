drop database if exists verkkokauppa;
create database verkkokauppa;
use verkkokauppa;

create table asiakas (
  id int primary key auto_increment,
  etunimi varchar(50) not null,
  sukunimi varchar(100) not null,
  lahiosoite varchar(100),
  postinumero char(5),
  postitoimipaikka varchar(100),
  email varchar(255),
  puhelin varchar(20)
);
create table login (
  id int primary key auto_increment,
  username varchar(50) not null unique,
  password varchar(255) not null,
  rights int not null
);

create table tuoteryhma (
  id int primary key auto_increment,
  nimi varchar(255) not null unique
);

create table tuote (
  id int primary key auto_increment,
  nimi varchar(255) not null,
  hinta decimal(5,2) not null,
  kuvaus text,
  varastomaara int not null,
  kuva varchar(50),
  tuoteryhma_id int not null,
  index (tuoteryhma_id),
  foreign key (tuoteryhma_id) references tuoteryhma(id)
  on delete restrict
);

create table tilaustilaus (
  id int primary key auto_increment,
  etunimi varchar(50) not null,
  sukunimi varchar(50) not null,
  puhelin varchar(20),
  email varchar(255),
  tilattu timestamp default current_timestamp
);

create table tilaus (
  id int primary key auto_increment,
  tila enum ('tilattu','toimitettu','maksettu'),
  tilattu timestamp default current_timestamp,
  asiakas_id int not null,
  index (asiakas_id),
  foreign key (asiakas_id) references asiakas(id)
  on delete restrict
);

create table tilausrivi (
  tilaus_id int not null,
  index (tilaus_id),
  foreign key (tilaus_id) references tilaus(id)
  on delete restrict,
  tuote_id int not null,
  index (tuote_id),
  foreign key (tuote_id) references tuote(id)
  on delete restrict,
  maara smallint
);


INSERT INTO tuoteryhma VALUES
('1', 'Köynnöskasvit'),
('2', 'Ikivihreät'),
('3', 'Viherkasvit'),
('4', 'Ruukkukukat');

INSERT INTO tuote VALUES
('1', 'Humala', '16.00', 'Nopeakasvuinen köynnöskasvi. Viihtyy sekä auringossa että varjossa, parhaiten puolivarjossa kosteahkossa maaperässä. Leikataan alas keväällä.

Hoito-ohjeet: Kasville annetaan tukea esim. säleiköllä. Tarvitsee vain kevyttä kuolleiden tai katkenneiden oksien harvennusta. Jos alkaa kasvaa liian suureksi, voidaan harventaa tai leikata alas jotta kasvu alkaisi uudelleen. Lannoitetaan täys- tai luonnonlannoitteella. Kastellaan pitkän kuivuuden jälkeen.', '52', 'humala.jpg', '1'),
('2', 'Muratti', '17.45', 'Helppohoitoinen ja vaatimaton pitkäversoinen kasvi. Ei välttämättä talvehdi Suomessa. Myrkyllinen

Hoito-ohjeet: Multa saa pysyä tasaisen kosteana. Pitää suihkuttelusta.', '12', 'muratti.jpg', '1'),
('3','Kataja', '13.47', 'Matala, leveä ja pensasmainen kasvutapa. Pirtsakan vihreät neulaset. Kestää kuivuutta.

Hoito-ohjeet: Ensimmäisellä kasvukaudella kastellaan säännöllisesti n. 1-3 viikon väliajoin. Lannoitetaan keväällä havukasvien erikoislannoitteella tai luonnonlannoitteella. Leikkaus rajoittuu latvan katkaisemiseen sopivalle korkeudelle tai kasvun rajoittamiseen. Kuivina kausina ja syksyllä kastelu on tärkeää.', '28', 'kataja.jpg', '2'),
('4', 'Alppiruusu', '20.40', 'Hieno, pyöreämallinen ja kompaktisti kasvava kääpiöpensas.

Hoito-ohjeet: Voidaan leikata. Viihtyy läpäisevässä, multavassa, kosteuden säilyttävässä ja happamassa kasvualustassa.', '13', 'alppiruusu.jpg', '2'),
('5', 'Rahapuu', '9.25', 'Helppohoitoinen mehikasvi kauniin vihreillä lehdillä. Vaatii vähän hoitotoimia.

Hoito-ohjeet: Selviää parikin viikkoa ilman kastelua.', '64', 'rahapuu.jpg', '3'),
('6', 'Rönsylilja', '8.00', 'Eritäin helppohoitoinen kasvi. Viihtyy valoisalla ja puolivarjoisalla paikalla.

Hoito-ohjeet: Kastellaan kun multa on vielä kosteaa. Pitää korkeasta ilmankosteudesta. Viihtyy hyvin kostealla kylpyhuoneen ikkunalla. Ruukutus joka toinen kevät.', '44', 'ronsylilja.jpg','3'),
('7', 'Kärsimyskukka', '18.15', 'Nopeakasvuinen kiipijäkasvi jolla on näyttävät kukat. Kestää aurinkoa, sekä pientä syyspakkasta.

Hoito-ohjeet: Kastellaan säännöllisesti. Uusien nuppujen kehittymiseen vaaditaan alle 18 C yölämpötila. Siirrä talveksi lepäämään 5-10 C asteeseen.', '21', 'karsimyskukka.jpg', '4'),
('8', 'Timanttiananas', '13.00', 'Helppohoitoinen ja upea trooppinen kasvi.

Hoito-ohjeet: Viihtyy puolivarjossa, ei pidä suorasta auringonvalosta. Lehtisuppilossa pitää aina olla vähän vettä. Kukkii usein 3kk. Muodostaa sivuruusukkeita tai versoja, jotka voidaan irrottaa ja istuttaa uusiksi kasveiksi keväällä.', '76', 'timanttiananas.jpg', '4' )