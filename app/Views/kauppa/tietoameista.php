<div class="row mt-3">
    <div class="col-md-4" id="tietoateksti">
        <h3 class="text-left">Köynnöskellarin tarina</h3>
        <br>
        <p class="text-left">
        Olemme vuonna 2020 perustettu oululainen yritys.
        </p>
        <p>
        Haluamme, että jokaisella
        on mahdollisuus nauttia kasvien kauneudesta, siksi perustimme yrityksen, joka
         toimittaa viherkasveja kaikkialle Suomeen.
        </p>
        <p>
       Yrittäjinä toimii viisi viherpeukaloa. Kaikilla on puutarha-alan koulutus,
        joten voit olla varma, että saat aina ammattitaitoista palvelua.
        </p>
        <p>
        Toivottavasti viihdyt kaupassamme!
        </p>
        <p>
        -Köynnöskellarin väki
        </p>
    </div>
    <div class="col-md-6">
    <img id="kukkanen" src="/img/tietoameista.jpg">
    </div>
</div>