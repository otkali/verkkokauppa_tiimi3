
<div class="row">
<div class="col-md-5" id="rekisteroidy">
<form action="/login/registration">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
        <div class="form-group">
        <label class="loginlabel">Nimi</label>
        <input
        class="form-control"
        name="nimi"
        placeholder="Etunimi"
        maxlength="30">
        <br>
        <input
        class="form-control"
        name="snimi"
        placeholder="Sukunimi"
        maxlength="30">
</div>
<div class="form-group">
        <label class="loginlabel">Yhteystiedot</label>
        <input
        class="form-control"
        name="sposti"
        placeholder="Sähköpostiosoite"
        maxlength="30">
        <br>
        <input
        class="form-control"
        name="puh"
        placeholder="Puhelinnumero"
        maxlength="30">
</div>
<div class="form-group">
        <label class="loginlabel">Kirjautumistiedot</label>
        <input
        class="form-control"
        name="user"
        placeholder="Käyttäjätunnus"
        maxlength="30">
        <br>
        <input
        class="form-control"
        name="password"
        type="password"
        placeholder="Salasana"
        maxlength="255">
        <br>
        <input
        class="form-control"
        name="confirmpassword"
        type="password"
        placeholder="Vahvista salasana"
        maxlength="255">
</div>
        <button class="btn btn-success">Rekisteröidy</button>
</form>
</div>

<div class="col-md-5" id="rekisteritietoa">
    <h3>Miksi rekisteröityminen kannattaa?</h3>
    <p>Rekisteröitymällä voit hyödyntää parhaiten kaikkia verkkokaupan 
        sinulle tarjoamia ominaisuuksia.
        <br>
        Esimerkiksi tilaaminen on nopeaa ja 
        vaivatonta, sillä omat yhteystietosi säilyvät verkkokaupan muistissa.
</p>
<p>
        Rekisteröityminen säästää aikaasi ja on maksutonta!
    </p>
</div>
</div>