<div class="row mt-3">
    <div class="col-md-12" style="text-align: center">
     
        <h1 class="ryhma">Köynnöskasvit</h1>
    
    </div>
</div>
<div class="row" id="tuote">
    <?php foreach ($tuotteet as $tuote): ?>
        <div class="col-md-3">
           <?php echo '<a href="'.base_url().'/tuote/'.$tuote['kuva'].'" data-lightbox="asd">
        <img class="kukkakuva" src="'.base_url().'/tuote/'.$tuote['kuva'].'" />';?>
    </a>
    
    </div>
        <div class="col-md-9 kukka" style="text-align: center">
        <form method="post" action="<?= site_url('ostoskori/lisaaKoynnos/' . $tuote['id']);?>">
            <h2 id="nimi"><?= $tuote['nimi'] ?></h2>
            <p id="hinta"><?= $tuote['hinta'] ?>€</p>
            <p id="kuvaus"><?= $tuote['kuvaus'] ?></p>
            <button class="navbarlinkki">Lisää ostoskoriin</button>
        </div>
        </form>
    <?php endforeach;?>
</div>