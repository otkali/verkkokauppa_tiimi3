<div >
  <form method="POST" action="/TilausController/create" class="marginstuff">
  <div class="form-group">
    <label for="enimi">Etunimi</label>
    <input type="text" class="form-control"  name="enimi" id="enimi" aria-describedby="" placeholder="Syötä etunimi" required>
  </div>
  <div class="form-group">
    <label for="snimi">Sukunimi</label>
    <input type="text" class="form-control" name="snimi" id="snimi" aria-describedby="" placeholder="Syötä sukunimi" required>
  </div>
  <div class="form-group">
    <label for="pnum">Puhelin</label>
    <input type="number" class="form-control" name="pnum" id="pnum" aria-describedby="" placeholder="Syötä puhnumero" required>
  </div>
  <div class="form-group">
    <label for="sposti">Sposti</label>
    <input type="email" class="form-control" name="sposti" id="sposti" aria-describedby="" placeholder="Syötä sposti" required>
  </div>   

  <button type="Tilaa" class="btn btn-success">Tilaa</button>
  </form>
</div>