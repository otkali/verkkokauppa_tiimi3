<div class="row marginstuff">
  <div class="col-6">
    <h3>Tuotteet</h3>
    <table>
      <tr>
        <th>ID</th>
        <th>Tuotteen nimi</th>
        <th>Tuoteryhmän ID</th>
      </tr>
      <?php foreach ($tuotteet as $tuote): ?>
      <tr>
        <td><?= $tuote['id'] ?></td>
        <td><?= $tuote['nimi'] ?></td>
        <td><?= $tuote['tuoteryhma_id'] ?></td>
        <td><button>Muokkaa</button></td>
        <td><?= anchor('AdminController/delete/' . $tuote['id'], 'delete') ?></td>
      </tr>
      <?php endforeach;?>
    </table>
  </div>
  <div class="col-6">
<form action="/AdminController/create" enctype="multipart/form-data" method="POST">
  <div class="form-group">
    <label for="tnimi">Tuotteen nimi</label>
    <input type="text" class="form-control"  name="tnimi" id="tnimi" aria-describedby="" placeholder="Syötä tuotteen nimi" required>
  </div>
  <div class="form-group">
    <label for="thinta">Tuotteen hinta</label>
    <input type="number" class="form-control" name="thinta" id="thinta" aria-describedby="" placeholder="Syötä tuotteen hinta" required>
  </div>
  <div class="form-group">
    <label for="tkuvaus">Tuotteen kuvaus</label>
    <textarea type="text" class="form-control" name="tkuvaus" id="tkuvaus" rows="4" aria-describedby="" placeholder="Syötä tuotteen kuvaus" required></textarea>
  </div>
  <div class="form-group">
    <label for="tvarasto">Tuotteen varastomäärä</label>
    <input type="number" class="form-control" name="tvarasto" id="tvarasto" aria-describedby="" placeholder="Syötä tuotteen varastomaara" required>
  </div>   
  <label for="tryhma">Tuoteryhmä num</label>
    <select class="form-control" name="tryhma" id="tryhma">
      <option value="1">1-Köynnöskasvit</option>
      <option value="2">2-Ikivihreät</option>
      <option value="3">3-Viherkasvit</option>
      <option value="4">4-Ruukkukukat</option>  
    </select>
  
  <div class="form-group">
    <label for="tkuva">Lisää tuotekuva</label>
    <input type="file" class="form-control-file" name="tkuva" id="tkuva1">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  </div>
  </div>