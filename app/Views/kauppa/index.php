<div class="row mt-3">
    <div class="col-md-7 marginstuff">
    <h1 class="mb-3 mt-3">Tervetuloa!</h1>
        
        <p>
        Köynnöskellari on laadukas huone- ja ulkokasveja myyvä verkkokauppa.
        </p>
        <p>
        Toimipisteemme sijaitsee Oulussa, mutta toimitamme kasvit
        <br>
         lähelle sinua kaikkialle Suomeen!
        </p>
        <img src="/img/kuva4.jpg" class="kuva1 img-fluid" alt="Responsive image">
        <p>
        Laatu ja ammattitaito ovat meille tärkeitä arvoja, siksi kasvimme tulevat luotettavilta toimittajilta ja ovat aina laadukkaita.
        Saat myös kasvien mukana hoito-ohjeet, joita noudattamalla kasvisi pysyy kauniina ja eloisana pitkään.
        </p>
        <p>
        Huone- ja ulkokasvit eivät vain näytä hyviltä, vaan niillä on myös positiivinen vaikutus mielialaan. 
        Katsele siis rauhassa tarjontaamme, tilaa hyvää energiaa kotiisi ja ota meihin yhteyttä, jos tulee kysyttävää. 
        </p>
        <img src="/img/indexkuva.jpg" class="kuva1 img-fluid" alt="Responsive image">
        
    </div>
</div>

