<div class="row">
    <div class="col marginstuff">
        <h1 class="mb-3 mt-3 ostoskori">Ostoskori</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-12 marginstuff mt-3">
      <table class="kori">
        <?php $summa = 0; ?>
        <?php foreach ($tuotteet as $tuote): ?>
          <tr>
          <form method="post" action="<?= site_url('ostoskori/poista/' . $tuote['id']);?>">
            <td width="30%"><?= $tuote['nimi'] ?></td>
            <td width="10%"><?= number_format(( $tuote['hinta'] * $tuote['maara']), 2, '.', ',') ?>€</td>
            <td class="kpl"><?= $tuote['maara'] ?> kpl</td>
            <td><button style="border:hidden;"><i class="fa fa-times" aria-hidden="true"></i></button></td>
          </form>
          </tr>

          <?php $summa += $tuote['hinta'] * $tuote['maara']; ?> 
          
        <?php endforeach; ?>
          <tr>
            <td class="bold">Yhteensä:</td>
            <td class="bold" colspan="2" style="text-align:left;"><?= number_format($summa, 2, '.', ',') ?> €</td>
            <td>&nbsp;</td>
          </tr>
      </table>
    </div>
    <div class="col-lg-9 tyhjenna">
    <div>
      <?= anchor('ostoskori/tyhjenna','Tyhjennä', 'class="musta"'); ?>
    
    <!-- <?= anchor('ostoskori/tilaa','Tilaa'); ?> -->
    </div>
        </div>
        <div class="col-md-6 tilausformi">
  <form method="POST" action="/TilausController/create" class="marginstuff ">
  <div class="form-group">
    <label for="enimi">Etunimi</label>
    <input type="text" class="form-control"  name="enimi" id="enimi" aria-describedby="" placeholder="Syötä etunimi" required>
  </div>
  <div class="form-group">
    <label for="snimi">Sukunimi</label>
    <input type="text" class="form-control" name="snimi" id="snimi" aria-describedby="" placeholder="Syötä sukunimi" required>
  </div>
  <div class="form-group">
    <label for="osoite">Lähiosoite</label>
    <input type="text" class="form-control" name="osoite" id="osoite" aria-describedby="" placeholder="Syötä osoite" required>
  </div>
  <div class="form-group">
    <label for="postin">Postinumero</label>
    <input type="text" class="form-control" name="postin" id="postin" aria-describedby="" placeholder="Syötä postinumero" required>
  </div>
  <div class="form-group">
    <label for="ptp">Postitoimipaikka</label>
    <input type="text" class="form-control" name="ptp" id="ptp" aria-describedby="" placeholder="Syötä postitoimipaikka" required>
  </div>
  <div class="form-group">
    <label for="pnum">Puhelin</label>
    <input type="number" class="form-control" name="pnum" id="pnum" aria-describedby="" placeholder="Syötä puhnumero" required>
  </div>
  <div class="form-group">
    <label for="sposti">Sposti</label>
    <input type="email" class="form-control" name="sposti" id="sposti" aria-describedby="" placeholder="Syötä sposti" required>
  </div>   

  <button type="Tilaa" class="btn btn-success">Tilaa</button>
  </form>
</div>
</div>