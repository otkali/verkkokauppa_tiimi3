
<form action="/login/check">
<div class="col-md-5" id="login">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class="form-group">
        <label class="loginlabel">Käyttäjätunnus</label>
        <input
        class="form-control"
        name="user"
        placeholder="syötä käyttäjätunnus"
        maxlength="30">
        </div>
        <div class="form-group">
        <label class="loginlabel">Salasana</label>
        <input
        class="form-control"
        name="password"
        type="password"
        placeholder="syötä salasana"
        maxlength="30">
        </div>
        <button class="btn btn-success">Kirjaudu</button>
        <?= anchor('kauppa/register','Rekisteröidy') ?>
</from>
</div>