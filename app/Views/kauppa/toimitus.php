<div class="row mt-3">
    <div class="col-md-7" id="kysymykset">
        <h3 class="text-left">Toimitus- ja maksutavat</h3>
        <h5>Toimitustavat</h5>
        <p>Voit valita toimitustavaksi Posti noutopisteeseen toimittamisen tai 
            Postin kotipaketin kotiovelle.
        </p>
        <p>
            Toimitusmaksu määräytyy tilauksen painon mukaan ja tilaus 
            pyritään aina toimittamaan yhtenä toimituksena.
        </p>
        <p>
            Tilauksen yhteydessä tulee aina ilmoittaa vastaanottajan 
            puhelinnumero.
        </p>
        <h5>Maksutavat</h5>
        <p>Voit maksaa Köynnöskellarin verkkokaupassa luottokortilla, verkkopankin 
            kautta suoraveloituksella tai laskulla. Ostaessasi laskulle sinun tulee
             olla yli 18-vuotias.
        </p>
        <p>
            Laskulla maksaminen on turvallista ja helppoa. Saat tilaamasi 
            tuotteet ennen kuin maksat niitä, maksuaika on 14 vuorokautta
        </p>
        <br>
        <img src="/img/toimitus.jpg" class="tiedot img-fluid" alt="Responsive image">
    </div>
</div>  