<div class="row mt-3">
    <div class="col-md-7" id="kysymykset">
        <h3 class="text-left">Usein kysytyt kysymykset</h3>
        <h5>Voinko tiltata tuotteen lahjaksi?</h5>
            <p>
               Voit halutessasi ilmoittaa tuotteelle eri toimitusosoitteen,
                kuin rekisteröityessäsi ilmoittamasi osoitteen.
            </p>
            <h5>Voinko tilata tuotteita ulkomaille?</h5>
            <p>
                Toimitamme tilauksia vain Suomeen.
            </p>
            <h5>Miten tilaukseni toimitetaan?</h5>
            <p>
                Tilaus toimitetaan tilausvaiheessa valitsemallasi toimitustavalla.
                Lue lisää kohdasta Toimitus- ja maksutavat.
            </p>
            <h5>Voinko palauttaa tilaukseni?</h5>
            <p>
                Voit palauttaa tilaamasi tuotteet 14 vuorokauden kuluessa tilauksen 
                vastaanottamisesta. Huolehdithan, että tuotteet ovat alkuperäispakkauksissaan
                ja myyntikelpoisia.
            </p>
            <h5>Miksi valikoima on niin pieni?</h5>
            <p>
                Olemme vasta aloittanut yritys, mutta pyrimme kasvattamaan valikoimaamme 
                koko ajan enemmän. Otamme mielellämme vastaan tuote-ehdotuksia!
            </p>
            <img src="/img/kysymykset.jpg" class="kuva1 img-fluid" alt="Responsive image">
        
    </div>
</div>