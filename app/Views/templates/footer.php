
<footer class="footer bottom row">
<div class="footer1 col-4">
<h4 class="smnautto">Köynnöskellari Oy</h4>
<a href="/kauppa/tietoameista" class="dropdownlinkki">Tietoa meistä</a><br>
<a href="/kauppa/kysymykset" class="dropdownlinkki">Usein kysyttyä</a>
</div>
<div class="footer1 col-4">
<h4>Seuraa meitä</h4>
<i class="fab fa-facebook-square fa-2x"></i>
<i class="fab fa-instagram-square fa-2x"></i>
<i class="fab fa-twitter-square fa-2x"></i>
<i class="fab fa-blogger fa-2x"></i>
</div>
<div class="footer1 col-4">
<h4 class="smnautto">Asiakaspalvelu</h4>
<a href="/kauppa/yhteystiedot" class="dropdownlinkki">Ota Yhteyttä</a><br>
<a href="/kauppa/toimitus" class="dropdownlinkki">Toimitus- ja maksutavat</a>
</div>
</footer>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script type="text/javascript" src="js/lightbox.js"></script>
  </body>
</html>