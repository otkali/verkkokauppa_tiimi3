<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/template.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/index.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/tietoameista.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/login.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/kukkasivut.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/kysymykset.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/toimitus.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/ostoskori.css'); ?>">
  <link href="https://fonts.googleapis.com/css?family=Inter&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Overlock+SC&display=swap" rel="stylesheet">
  <title>Köynnöskellari</title>
</head>
<div class="container-fluid padding">
<nav class="navbar navbar-expand-md">
  <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a href="/kauppa">
        <img class="nav-link" id="navbarlogo" src="/img/logo.png" >
        </a>
      </li>
    </ul>
  </div>
  <div class="mx-auto order-0">
        <div class="input-group col">
          <input type="search" id="navbarsearch" placeholder="Etsi" aria-describedby="button-addon5" class="form-control">
          <div class="input-group-append">
            <button id="navbar-searchbutton" type="submit" class="btn btn-success"><i class="fas fa-search"></i></button>
          </div>
        </div>
  </div>
  <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <button class="nav-link navbarlinkki" onclick="location.href='/kauppa/ostoskori'"><span class="fas fa-shopping-cart kuvake"></span> Ostoskori <span class="badge badge-light ostosluku">  
      <?php
    if (isset($_SESSION['kori'])) {
      echo '<div><a href="' . site_url('cart/index') . '">' . count($_SESSION['kori']) . '</a></div>';
      //echo 'Kori on olemassa';
      //print_r($_SESSION['kori']);
    }
    else {
      echo '<div>0</div>';
    }
    ?></span></button>
      </li>
      &nbsp;&nbsp;
      <li class="nav-item">
        <button class="nav-link navbarlinkki" href="#"><span class="fas fa-user-alt kuvake"></span> Profiili</button>
      </li>
    </ul>
  </div>
</nav>
<section class="row hederi">
  <div class="col padding">
  <a class="nav-link ylalinkki" href="/kauppa">Etusivu</a>
  </div>
  <div class="col padding">
  <a class="nav-link ylalinkki" href="#" data-toggle="dropdown">Ulkokasvit <span class="fas fa-chevron-down fa-sm nuoli"></span></a>
  <ul class="dropdown-menu dropdownmenut col">
    <li class="dropdownlinkkilista"><a class="dropdownlinkki" href="/kauppa/koynnos">Köynnöskasvit</a></li>
    <li class="dropdownlinkkilista"><a class="dropdownlinkki" href="/kauppa/ikivihree">Ikivihreät</a></li>
  </ul>
  </div>
  <div class="col padding">
  <a class="nav-link ylalinkki" href="#" data-toggle="dropdown">Huonekasvit <span class="fas fa-chevron-down fa-sm nuoli"></span></a>
  <ul class="dropdown-menu dropdownmenut col">
    <li><a class="dropdownlinkki" href="/kauppa/viherkasvi">Viherkasvit</a></li>
    <li><a class="dropdownlinkki" href="/kauppa/ruukku">Ruukkukukat</a></li>
  </ul>
  </div>
  <div class="col padding">
  <a class="nav-link ylalinkki" href="#" data-toggle="dropdown"><span class="fas fa-user-lock"></span> Admin <span class="fas fa-chevron-down fa-sm nuoli"></span></a>
  <ul class="dropdown-menu dropdownmenut col">
    <li><a class="dropdownlinkki" href="/AdminController/adminpage">Tuotteiden muokkaus</a></li>
    <li><a class="dropdownlinkki" href="/AdminController/logout">Kirjaudu ulos</a></li>
  </ul>
  </div>
</section>
