<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {
    protected $table = 'login';

    protected $allowedFields = ['username','password'];

    public function check($username,$password) {
        $this->where('username', $username);
        $query = $this->get();
        // print $this->getLastQuery();
        $row = $query->getRow();
        if ($row) {
            if (password_verify($password,$row->password)) {
                return $row;
            }
        }
        return null;
    }
    public function checkAdmin($username) {
        $this->where('username', $username);
        $query = $this->get();
        // print $this->getLastQuery();
        $row = $query->getRow();
        if ($row->rights) {
            return 1;
        }
        return 0;
    }
}
?>