<?php namespace App\Models;

use CodeIgniter\Model;
class KukkaModel extends Model {
    protected $table = 'tuote';
    public function getTuotteet() {
        return $this->findAll();
    }

    public function haeKukat($idt) {
        $result = array();
        foreach ($idt as $id){
            $this->table('tuote');
            $this->select('id, nimi, hinta, kuvaus, kuva');
            $this->where('id', $id);
            $query = $this->get();

            $product = $query->getRowArray();
            $this->add_product_to_array($product,$result);

            $this->resetQuery();
        }

        return $result;
    }

    private function add_product_to_array($product,&$array){
        for($i = 0;$i < count($array);$i++){
          if ($array[$i]['id'] === $product['id']){
              $array[$i]['maara'] = $array[$i]['maara'] + 1;
              return;
          }
        }
        $product['maara'] = 1;
        array_push($array,$product);
    }
    public function haeTuoteryhmalla($tuoteryhma_id){
        $builder = $this->table('tuote');
        $query = $builder->getWhere(['tuoteryhma_id' => $tuoteryhma_id]);
        return $query->getResultArray();
    }
}
