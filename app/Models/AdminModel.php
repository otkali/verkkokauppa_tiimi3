<?php namespace App\Models;

use CodeIgniter\Model;

class AdminModel extends Model {
    protected $table = 'tuote';

    protected $allowedFields = ['id','nimi','hinta','kuvaus','varastomaara','kuva','tuoteryhma_id'];

    
    public function remove($id) {
        $this->where('id',$id);
        $this->delete();
    }
}
?>