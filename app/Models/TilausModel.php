<?php namespace App\Models;

use CodeIgniter\Model;

use App\Models\AsiakasModel;
use App\Models\TilausriviModel;

class TilausModel extends Model {
    /*protected $table = 'tilaustilaus';

    protected $allowedFields = ['id','etunimi','sukunimi','puhelin','email','tilattu']; */

    protected $table = 'tilaus';

    protected $allowedFields = ['asiakas_id', 'tila'];

    public function tallenna($asiakas, $ostoskori) {
        
        $this->db->transStart();

        $asiakas_id = $this->tallennaAsiakas($asiakas);

        $tilaus_id = $this->tallennaTilaus($asiakas_id);

        $this->tallennaTilausrivit($tilaus_id, $ostoskori);
        
        $this->db->transComplete();

       /* if($this-transStatus() === FALSE) {

        } */
    }

    
    public function remove($id) {
        $this->where('id',$id);
        $this->delete();
    }

    private function tallennaAsiakas($asiakas) {
        $asiakasModel = new asiakasModel();
        $asiakasModel->save($asiakas);
        return $this->insertID();
    }
    private function tallennaTilaus($asiakas_id) {
        $this->save([
            'asiakas_id' => $asiakas_id,
            'tila' => 'tilattu'
        ]);
        return $this->insertID();
    }
    private function tallennaTilausrivit($tilaus_id, $ostoskori) {
        $tilausriviModel = new TilausriviModel;

        foreach($ostoskori as $tuote_id) {

            $tilausriviModel->save([
                'tilaus_id' => $tilaus_id,
                'tuote_id' => $tuote_id,
                'maara' => 1
            ]);

        }
    }
}
?>