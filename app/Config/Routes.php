<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('kauppa','Kauppa::Index');
$routes->get('kauppa/koynnos','Kauppa::koynnos');
$routes->get('kauppa/ikivihree','Kauppa::ikivihree');
$routes->get('kauppa/ruukku','Kauppa::ruukku');
$routes->get('kauppa/viherkasvi','Kauppa::viherkasvi');
$routes->get('kauppa/ostoskori','Ostoskori::index');
$routes->get('login', 'Login::index');
$routes->get('kauppa/login', 'Kauppa::login');
$routes->get('admincontroller/admin', 'AdminController::index', ['as' => 'admin']);
$routes->get('kauppa/adminpage', 'Kauppa::adminpage');
$routes->get('AdminController/adminpage', 'AdminController::adminpage');
$routes->get('TilausController/kiitos','TilausController::kiitos');


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
