<?php namespace App\Controllers;

use App\Models\KukkaModel;
use App\Models\AdminModel;

class AdminController extends BaseController{

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
    }

    public function index() {

        if (!isset($_SESSION['user'])) {
            return redirect('kauppa/login');
        }

        echo view('templates/adminheader');
        echo view('kauppa/index');
        echo view('templates/footer');
    }

    public function adminpage() {
        $model = new KukkaModel();
        $data['tuotteet'] = $model->getTuotteet();
        echo view('templates/adminheader',$data);
        echo view('kauppa/adminpage',$data);
        echo view('templates/footer',$data);
    }

    public function logout() {
        session_unset();
        session_destroy();
        return redirect('kauppa');
    }

    public function delete($id) {
        if (!is_numeric($id)) {
            throw new \Exception('Provided id is not a number.');
        }
        
        $model = new AdminModel();

        $model->remove($id);
        return redirect('AdminController/adminpage');
    }

    public function create() {
        $model = new AdminModel();

        if (!$this->validate([
            'tnimi' => 'required|max_length[255]',
        ])){
            echo view('templates/adminheader');
            echo view('AdminController/adminpage');
            echo view('templates/footer');
        } else {
            //echo $polku;
            //print_r($this->request);

            $file = $this->request->getFile('tkuva');
            $name = $file->getName();
            //print $name;
            //print $file;
            //$upload_data = $this->request->getFiles();
            //print_r($upload_data->getName());
            
            $user = $_SESSION['user'];
            $tkuva = $this->request->getFile('tkuva');
            $polku = APPPATH;
            $polku = str_replace('app','public\tuote', $polku);
            $tkuva->move($polku);
            //print_r ($polku);
            //print "Nimi on = $name ";

            $model->save([
                'nimi' => $this->request->getVar('tnimi'), 
                'hinta' => $this->request->getVar('thinta'),
                'kuvaus' => $this->request->getVar('tkuvaus'),
                'varastomaara' => $this->request->getVar('tvarasto'),
                'tuoteryhma_id' => $this->request->getVar('tryhma'),
                'kuva' => $name
            ]);
            return redirect('AdminController/adminpage');
        }
    }
}