<?php namespace App\Controllers;

use App\Models\LoginModel;

const REGISTER_TITLE = 'Köynnöskellari - Register';
const LOGIN_TITLE = "Köynnöskellari - Login";

class Login extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
    }

    public function index() {
        $data['title'] = 'Todo - Login';
        echo view('templates/header',$data);
        echo view('login/login',$data);
        echo view('templates/footer',$data);
    }

    public function register() {
        $data['title'] = REGISTER_TITLE;
        echo view('templates/header',$data);
        echo view('login/register',$data);
        echo view('templates/footer',$data);
    }

    public function registration() {
        $model = new LoginModel();

        if(!$this->validate([
            'user' => 'required|min_length[8]|max_length[30]',
            'password' => 'required|min_length[8]|max_length[30]',
            'confirmpassword' => 'required|min_length[8]|max_length[30]|matches[password]',
        ])){
            echo view('templates/header' , ['title' => REGISTER_TITLE]);
            echo view('kauppa/register');
            echo view('templates/footer');
        }
        else {
            $model->save([
                'username' => $this->request->getVar('user'),
                'password' => password_hash($this->request->getVar('password'),PASSWORD_DEFAULT),
            ]);
            return redirect('kauppa/login');
            //print "rekisteröinti onnistui";
        }
    }

    public function check() {
        $model = new LoginModel();

        if(!$this->validate([
            'user' => 'required|min_length[8]|max_length[30]',
            'password' => 'required|min_length[8]|max_length[30]',
        ])){
            echo view('templates/header' , ['title' => LOGIN_TITLE]);
            echo view('kauppa/login');
            echo view('templates/footer');
        }
        else {
            $user = $model->check(
                $this->request->getVar('user'),
                $this->request->getVar('password'),
                $this->request->getVar('rights')
            );
            if ($user) {
                $_SESSION['user'] = $user;
                $_SESSION['rights'] = $model->checkAdmin($user->username);
                if($_SESSION['rights']==1) {
            
                return redirect('admin');

                }
                else {
                    return redirect('kauppa/login');
                    //redirect näkymälle mikä on ei adminille
                }
            }
            else {
                return redirect('kauppa/login');
            }
        }
    }
}

?>