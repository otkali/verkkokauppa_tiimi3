<?php namespace App\Controllers;

use App\Models\KukkaModel;
use App\Models\AdminModel;
use App\Models\TilausModel;


class TilausController extends BaseController{

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
    }

    public function kiitos(){
        echo view('templates/header');
        echo view('kauppa/kiitospage');
        echo view('templates/footer');
    }

    public function create() {
        /*$model = new TilausModel();

        if (!$this->validate([
            'enimi' => 'required|max_length[255]',
        ])){
            echo view('templates/header');
            echo view('kauppa/tilaus');
            echo view('templates/footer');
        } else {
            //$user = $_SESSION['user'];
            $model->save([
                'etunimi' => $this->request->getVar('enimi'), 
                'sukunimi' => $this->request->getVar('snimi'),
                'puhelin' => $this->request->getVar('pnum'),
                'email' => $this->request->getVar('sposti')
            ]);
            $tilausModel = new TilausModel();
            
            $tilausModel->tallenna($_SESSION['kori']);

            return redirect('TilausController/kiitos');
        }*/
        $asiakas = [
            'etunimi' => $this->request->getPost('enimi'), 
            'sukunimi' => $this->request->getPost('snimi'),
            'lahiosoite' => $this->request->getPost('osoite'), 
            'postinumero' => $this->request->getPost('postin'),
            'postitoimipaikka' => $this->request->getPost('ptp'),    
            'puhelin' => $this->request->getPost('pnum'),
            'email' => $this->request->getPost('sposti')
        ];

        $tilausModel = new TilausModel();
            
        $tilausModel->tallenna($asiakas, $_SESSION['kori']);

        unset($_SESSION['kori']);

        return redirect('TilausController/kiitos');
    }
}
?>