<?php namespace App\Controllers;

use App\Models\KukkaModel;

class Ostoskori extends BaseController {

  private $kukkaModel = null;

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
        if (!isset($_SESSION['kori'])) {
          $_SESSION['kori'] = array();
        }
        $this->kukkaModel = new KukkaModel();
    }

    public function index() {
      if(count($_SESSION['kori']) > 0){
        $tuotteet = $this->kukkaModel->haeKukat($_SESSION['kori']);
      } else {
        $tuotteet = array();
      }

        $data['tuotteet'] = $tuotteet;
        echo view('templates/header');
        echo view('kauppa/ostoskori', $data);
        echo view('templates/footer');
    }

    public function lisaaKoynnos($tuote_id) {
    
        array_push($_SESSION['kori'],$tuote_id);
        return redirect('kauppa/koynnos');
      }

      public function lisaaIkivihree($tuote_id) {
    
        array_push($_SESSION['kori'],$tuote_id);
        return redirect('kauppa/ikivihree');
      }

      public function lisaaRuukku($tuote_id) {
    
        array_push($_SESSION['kori'],$tuote_id);
        return redirect('kauppa/ruukku');
      }

      public function lisaaViherkasvi($tuote_id) {
    
        array_push($_SESSION['kori'],$tuote_id);
        return redirect('kauppa/viherkasvi');
      }

      public function poista($tuote_id){
        $index = array_search($tuote_id,$_SESSION['kori']);
          foreach($_SESSION['kori'] as $key=>$value){
            if($key === $index){
              array_splice($_SESSION['kori'], $index, 1);
            } 
          }
          return redirect('kauppa/ostoskori');
      }

    public function tyhjenna(){
      $_SESSION['kori'] = null;
      return redirect('kauppa/ostoskori');
    }

    public function tilaa() {
      echo view('templates/header');
      echo view('kauppa/tilaus');
      echo view('templates/footer');
    }
}