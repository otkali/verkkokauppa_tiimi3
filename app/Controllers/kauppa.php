<?php namespace App\Controllers;
use App\Models\KukkaModel;
class Kauppa extends BaseController {

    public function __construct() {
        $session = \Config\Services::session();
        $session->start();
    }

    public function index() {
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader');
                echo view('kauppa/index');
                echo view('templates/footer');
        } else {
            echo view('templates/header');
            echo view('kauppa/index');
            echo view('templates/footer');
            }  
        }
        else {
            echo view('templates/header');
            echo view('kauppa/index');
            echo view('templates/footer');
            }  
    }

    public function tietoameista() {
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader');
                echo view('kauppa/tietoameista');
                echo view('templates/footer');
        } else {
            echo view('templates/header');
            echo view('kauppa/tietoameista');
            echo view('templates/footer');
            }  
        }
        else {
            echo view('templates/header');
            echo view('kauppa/tietoameista');
            echo view('templates/footer');
            }  
    }

    public function koynnos() {
        $model = new KukkaModel();
        $tuoteryhma_id = '1';
        $data['tuotteet'] = $model->haeTuoteryhmalla($tuoteryhma_id);
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader',$data);
                echo view('kauppa/koynnos',$data);
                echo view('templates/footer',$data);
        } else {
            echo view('templates/header',$data);
            echo view('kauppa/koynnos',$data);
            echo view('templates/footer',$data);
            }  
        }
        else {
            echo view('templates/header',$data);
            echo view('kauppa/koynnos',$data);
            echo view('templates/footer',$data);
            }  
    }
    public function ikivihree() {
        $model = new KukkaModel();
        $tuoteryhma_id = '2';
        $data['tuotteet'] = $model->haeTuoteryhmalla($tuoteryhma_id);
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader',$data);
                echo view('kauppa/ikivihree',$data);
                echo view('templates/footer',$data);
        } else {
            echo view('templates/header',$data);
            echo view('kauppa/ikivihree',$data);
            echo view('templates/footer',$data);
            }  
        }
        else {
            echo view('templates/header',$data);
            echo view('kauppa/ikivihree',$data);
            echo view('templates/footer',$data);
            }  
    }

    public function viherkasvi() {
        $model = new KukkaModel();
        $tuoteryhma_id = '3';
        $data['tuotteet'] = $model->haeTuoteryhmalla($tuoteryhma_id);
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader',$data);
                echo view('kauppa/viherkasvi',$data);
                echo view('templates/footer',$data);
        } else {
            echo view('templates/header',$data);
            echo view('kauppa/viherkasvi',$data);
            echo view('templates/footer',$data);
            }  
        }
        else {
            echo view('templates/header',$data);
            echo view('kauppa/viherkasvi',$data);
            echo view('templates/footer',$data);
            }  
    }
    public function ruukku() {
        $model = new KukkaModel();
        $tuoteryhma_id = '4';
        $data['tuotteet'] = $model->haeTuoteryhmalla($tuoteryhma_id);
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader',$data);
                echo view('kauppa/ruukku',$data);
                echo view('templates/footer',$data);
        } else {
            echo view('templates/header',$data);
            echo view('kauppa/ruukku',$data);
            echo view('templates/footer',$data);
            }  
        }
        else {
            echo view('templates/header',$data);
            echo view('kauppa/ruukku',$data);
            echo view('templates/footer',$data);
            }  
    }

    public function login() {
        echo view('templates/header');
        echo view('kauppa/login');
        echo view('templates/footer');
    }

    public function register() {
        echo view('templates/header');
        echo view('kauppa/register');
        echo view('templates/footer');
    }

    public function kysymykset() {
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader');
                echo view('kauppa/kysymykset');
                echo view('templates/footer');
        } else {
            echo view('templates/header');
            echo view('kauppa/kysymykset');
            echo view('templates/footer');
            }  
        }
        else {
            echo view('templates/header');
            echo view('kauppa/kysymykset');
            echo view('templates/footer');
            }  
    }

    public function yhteystiedot() {
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader');
                echo view('kauppa/yhteystiedot');
                echo view('templates/footer');
        } else {
            echo view('templates/header');
            echo view('kauppa/yhteystiedot');
            echo view('templates/footer');
            }  
        }
        else {
            echo view('templates/header');
            echo view('kauppa/yhteystiedot');
            echo view('templates/footer');
            }  
    }


    public function toimitus() {
        if(isset($_SESSION['rights'])){
            if($_SESSION['rights']==1){
                echo view('templates/adminheader');
                echo view('kauppa/toimitus');
                echo view('templates/footer');
        } else {
            echo view('templates/header');
            echo view('kauppa/toimitus');
            echo view('templates/footer');
            }  
        }
        else {
            echo view('templates/header');
            echo view('kauppa/toimitus');
            echo view('templates/footer');
            }  
    }
}